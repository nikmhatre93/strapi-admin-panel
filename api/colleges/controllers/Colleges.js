'use strict';

/**
 * Colleges.js controller
 *
 * @description: A set of functions called "actions" for managing `Colleges`.
 */

module.exports = {

  /**
   * Retrieve colleges records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.colleges.search(ctx.query);
    } else {
      return strapi.services.colleges.fetchAll(ctx.query);
    }
  },

  /**
   * Retrieve a colleges record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.colleges.fetch(ctx.params);
  },

  /**
   * Count colleges records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.colleges.count(ctx.query);
  },

  /**
   * Create a/an colleges record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.colleges.add(ctx.request.body);
  },

  /**
   * Update a/an colleges record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.colleges.edit(ctx.params, ctx.request.body) ;
  },

  /**
   * Destroy a/an colleges record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.colleges.remove(ctx.params);
  }
};

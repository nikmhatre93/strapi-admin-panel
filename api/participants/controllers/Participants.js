'use strict';
var storage = require('node-persist')

/**
 * Participants.js controller
 *
 * @description: A set of functions called "actions" for managing `Participants`.
 */

module.exports = {

  /**
   * Retrieve participants records.
   *
   * @return {Object|Array}
   */

  find: async (ctx) => {
    if (ctx.query._q) {
      return strapi.services.participants.search(ctx.query);
    } else {
      return strapi.services.participants.fetchAll(ctx.query);
    }
  },

  findAll: async (ctx) => {
    return strapi.services.participants.fetchAll2(ctx.query);
  },

  /**
   * Retrieve a participants record.
   *
   * @return {Object}
   */

  findOne: async (ctx) => {
    if (!ctx.params._id.match(/^[0-9a-fA-F]{24}$/)) {
      return ctx.notFound();
    }

    return strapi.services.participants.fetch(ctx.params);
  },


  findOneByEmailOrNumber: async (ctx) => {

    return strapi.services.participants.fetchByParam(ctx.request.body);;
  },


  otp: async (ctx) => {

    await storage.init()
    var a = await storage.getItem(ctx.request.body.email)
    if (a == undefined) {
      var otp = Math.floor(1000 + Math.random() * 9000)
      await storage.setItem(ctx.request.body.email, otp)
      a = otp
    }

    return { email: ctx.request.body.email, otp: a };
  },

  verifyOtp: async (ctx) => {

    await storage.init()
    var a = await storage.getItem(ctx.request.body.email)
    if (a == ctx.request.body.otp) {
      await storage.removeItem(ctx.request.body.email)
      return { status: true, message: 'Successful' }
    } else {
      return { status: false, message: 'Failure' }
    }
  },

  /**
   * Count participants records.
   *
   * @return {Number}
   */

  count: async (ctx) => {
    return strapi.services.participants.count(ctx.query);
  },

  /**
   * Create a/an participants record.
   *
   * @return {Object}
   */

  create: async (ctx) => {
    return strapi.services.participants.add(ctx.request.body);
  },

  /**
   * Update a/an participants record.
   *
   * @return {Object}
   */

  update: async (ctx, next) => {
    return strapi.services.participants.edit(ctx.params, ctx.request.body);
  },

  /**
   * Destroy a/an participants record.
   *
   * @return {Object}
   */

  destroy: async (ctx, next) => {
    return strapi.services.participants.remove(ctx.params);
  }
};
